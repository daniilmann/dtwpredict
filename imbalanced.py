def imbalance_bar(data, column, default_T=2, ewm_span=16):

    imbars = pd.DataFrame(columns=['Open', 'High', 'Low', 'Close', 'Volume'])

    data = data.copy()
    data['BS'] = np.sign(data[column].diff()).replace(0.0, np.nan).fillna(method='pad')
    # data['BSV'] = data.BS * data[column]
    data['BSV'] = np.log(data[column] / data[column].shift()).fillna(0.0)
    data['BSVEWM'] = data.BSV.ewm(span=ewm_span).mean()
    data['BSVEWMA'] = data.BSVEWM.abs()

    data.fillna(0.0, inplace=True)

    Ts = [2]

    i = Ts[0]
    bar_len = 50
    idxs = [1, Ts[0]]
    while i < data.shape[0]:

        eT = pd.Series(Ts).ewm(span=ewm_span).mean().values[-1]
        abss = data.BSVEWMA.iloc[i-1]
        val = eT * abss

        theta = data.BSV.iloc[i:i + bar_len].cumsum().abs().values

        fslc = theta >= val
        if ~np.any(fslc):
            T = default_T
            # T = int(np.round(np.mean((np.argmin(np.abs(theta - val)), np.argmin(theta))) + 1))
        else:
            func_idxs = np.where(fslc)[0]
            func_val = theta[fslc]

            T = func_idxs[np.nanargmin(func_val)] + 1

        Ts.append(T)

        i += T
        idxs.append(i)

    if idxs[-1] >= data.shape[0]:
        idxs[-1] = data.shape[0] - 1

    for i0, i1 in zip(idxs[:-1], idxs[1:]):
        imbars.loc[data.index[i1]] = [
            data.iloc[i0].Open,
            data.iloc[i0:i1].High.max(),
            data.iloc[i0:i1].Low.min(),
            data.iloc[i1 - 1].Close,
            data.iloc[i0:i1].Volume.sum()
        ]

    rets = np.log(data.Close / data.Close.shift())[1:]
    irets = np.log(imbars.Close / imbars.Close.shift())[1:]

    # print(rets.describe())
    # print(irets.describe())

    return imbars