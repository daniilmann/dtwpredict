import tensorflow as tf
from tensorflow import keras
from keras.models import Model, Sequential
from keras.layers import Input, Dense, LSTM, Dropout, Reshape, Concatenate, Lambda, concatenate
# from keras.backend import concatenate
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tslearn.metrics import soft_dtw
from sklearn.preprocessing import MinMaxScaler
# from keras import backend as K
from pywt import dwt, idwt
from utils import get_data

def calculate_ab(period):
    pos_median_array = []
    neg_median_array = []
    i = 1
    while i < len(period):
        sum_array = period.rolling(i).sum()

        pos = [i for i in sum_array if i > 0]
        neg = [i for i in sum_array if i < 0]
        if len(pos) > 0:
            pos_median_array.append(np.percentile(pos, q=75))
        if len(neg) > 0:
            neg_median_array.append(np.percentile(neg, q=25))
        i += 1
    b, a = 0, 0
    if len(pos_median_array) > 0:
        b = np.percentile(pos_median_array, q=75)
    if len(neg_median_array) > 0:
        a = np.percentile(neg_median_array, q=25)
    return a, b


def yi(a, b, period_n, period):
    i = 0
    if period[i] > b and b != 0:
        return 1, period[:i + 1]
    if period[i] < a and a != 0:
        return 2, period[:i + 1]
    i = 1
    while i < period_n:
        if period[i] > b and b != 0:
            return 1, period[:i + 1]
        if period[i] < a and a != 0:
            return 2, period[:i + 1]
        i += 1
    return 0, period


class CreateData:
    def __init__(self):
        self.data = None
        self.sequence_len = 6
        self.train_data = []
        self.train_data_len = 0
        self.x_train = []
        self.y_train = []
        self.x_test = []
        self.y_test = []
        self.n = 5
        self.period_len = 6

    def create_train_and_test_data(self):
        i = self.sequence_len + self.n
        array = []
        print(self.train_data)
        while i < len(self.train_data):
            a = np.array(self.train_data[i - (self.sequence_len + self.n):i])
            array.append(a)
            i += self.sequence_len
        array = np.random.permutation(array)
        l = int(len(array) * 0.8)
        self.x_train = array[:l, :self.sequence_len]
        self.y_train = array[:l, self.sequence_len:]
        self.x_test = array[l:, :self.sequence_len]
        self.y_test = array[l:, self.sequence_len:]
        self.x_train, self.y_train = np.array(self.x_train), np.array(self.y_train)
        self.x_train = np.reshape(self.x_train, (self.x_train.shape[0], self.x_train.shape[1], 1))
        self.x_test, self.y_test = np.array(self.x_test), np.array(self.y_test)
        self.x_test = np.reshape(self.x_test, (self.x_test.shape[0], self.x_test.shape[1], 1))

    def create_train_test_data_ver2(self):
        i = 2
        array = []
        while i * self.period_len < len(self.train_data):
            period = self.train_data[self.period_len * (i - 1):self.period_len * i]
            a, b = calculate_ab(period)
            q, y = yi(a, b, self.period_len, np.array(period))
            if len(y) > 1:
                period = self.train_data[self.period_len * (i - 2):self.period_len * (i - 1)]
                array.append(list(period))
                array[-1].append(y)
            i += 1
        array = np.random.permutation(array)
        l = int(len(array) * 0.8)
        self.x_train = array[:l, :self.period_len]
        self.y_train = array[:l, self.period_len:]
        self.x_test = array[l:, :self.period_len]
        self.y_test = array[l:, self.period_len:]
        self.x_train = np.array(self.x_train)
        self.x_train = np.reshape(self.x_train, (self.x_train.shape[0], self.x_train.shape[1], 1))
        self.x_test = np.array(self.x_test)
        self.x_test = np.reshape(self.x_test, (self.x_test.shape[0], self.x_test.shape[1], 1))




def imbalance_bar(data, column, default_T=2, ewm_span=16):

    imbars = pd.DataFrame(columns=['Open', 'High', 'Low', 'Close', 'Volume'])

    data = data.copy()
    data['BS'] = np.sign(data[column].diff()).replace(0.0, np.nan).fillna(method='pad')
    # data['BSV'] = data.BS * data[column]
    data['BSV'] = np.log(data[column] / data[column].shift()).fillna(0.0)
    data['BSVEWM'] = data.BSV.ewm(span=ewm_span).mean()
    data['BSVEWMA'] = data.BSVEWM.abs()

    data.fillna(0.0, inplace=True)

    Ts = [2]

    i = Ts[0]
    bar_len = 50
    idxs = [1, Ts[0]]
    while i < data.shape[0]:

        eT = pd.Series(Ts).ewm(span=ewm_span).mean().values[-1]
        abss = data.BSVEWMA.iloc[i-1]
        val = eT * abss

        theta = data.BSV.iloc[i:i + bar_len].cumsum().abs().values

        fslc = theta >= val
        if ~np.any(fslc):
            T = default_T
            # T = int(np.round(np.mean((np.argmin(np.abs(theta - val)), np.argmin(theta))) + 1))
        else:
            func_idxs = np.where(fslc)[0]
            func_val = theta[fslc]

            T = func_idxs[np.nanargmin(func_val)] + 1

        Ts.append(T)

        i += T
        idxs.append(i)

    if idxs[-1] >= data.shape[0]:
        idxs[-1] = data.shape[0] - 1

    for i0, i1 in zip(idxs[:-1], idxs[1:]):
        imbars.loc[data.index[i1]] = [
            data.iloc[i0].Open,
            data.iloc[i0:i1].High.max(),
            data.iloc[i0:i1].Low.min(),
            data.iloc[i1 - 1].Close,
            data.iloc[i0:i1].Volume.sum()
        ]

    rets = np.log(data.Close / data.Close.shift())[1:]
    irets = np.log(imbars.Close / imbars.Close.shift())[1:]

    # print(rets.describe())
    # print(irets.describe())

    return imbars


data = get_data('./data', 'BTC', 'USD', '1h', 'CCCAGG', start_date='2019-01-01 00:00:00', update_data=False)


sequence_len = 6
df = CreateData()
df.data = data
# pd.read_csv('1h_BTC_USD_Bitstamp.csv')
df.sequence_len = sequence_len

df.n = 3

# df.data = imbalance_bar(data=df.data, column='Volumefrom', ewm_span=5)
ret = np.log(df.data.Close / df.data.Close.shift()).fillna(0.0)
print(len(ret))

"""plt.plot(ret, 'black')
plt.legend()
plt.show()"""

df.train_data = ret

df.period_len = 6

df.create_train_test_data_ver2()

'''
dret = dwt(dwt(ret.values, 'db20')[0], 'db20')[0]
dret = idwt(idwt(dret, None, 'db20'), None, 'db20')'''
# df.train_data_len = int(len(df.data['Close']) * 0.9)



# sc = MinMaxScaler(feature_range=(0, 1))
# ret = df.data.Close.fillna(method='pad').dropna()
# ret = np.log(ret / ret.shift()).fillna(0).cumsum().values.reshape(-1, 1)
# df.train_data = dret
# sc.fit(df.train_data)
# df.train_data = sc.transform(df.train_data)
# df.transform_data()
# df.create_train_data()
# df.create_test_data()
# df.create_train_and_test_data()
# print(df.x_train, df.train_data.shape)

period_num = 2

# print(len(df.y_train[0, 0]), df.y_train[0:10, 0])
# df.y_train = list(df.y_train[0:, 0])
# print(len(df.y_train[0]))

print('y_train[0] len', len(df.y_train[0]), df.y_train[0])

print('---')

class TrainModel:
    def __init__(self):
        self.model = None
        self.data = None
        # self.inputs = None
        # self.lstm1 = None
        self.yt = []
        self.yp = []
        self.n = 0
        self.N = 0

    def create_layers(self):
        inputs1 = Input(shape=(self.data.sequence_len, 1), name='input1')
        lstm0 = LSTM(units=self.N)(inputs1)
        lstm1 = LSTM(units=1, name='output1')(inputs1)
        # inputs2 = Concatenate()([lstm0, lstm1])
        inputs2 = concatenate([lstm0, lstm1])
        inputs2 = Reshape(target_shape=(self.N + 1, 1))(inputs2)
        lstm2 = LSTM(units=1, name='output2')(inputs2)
        # inputs3 = Concatenate()([lstm0, lstm1, lstm2])
        inputs3 = concatenate([lstm0, lstm1, lstm2])
        inputs3 = Reshape(target_shape=(self.N + 2, 1))(inputs3)
        lstm3 = LSTM(units=1, name='output3')(inputs3)
        # inputs4 = Concatenate()([lstm0, lstm1, lstm2, lstm3])
        inputs4 = concatenate([lstm0, lstm1, lstm2, lstm3])
        inputs4 = Reshape(target_shape=(self.N + 3, 1))(inputs4)
        lstm4 = LSTM(units=1, name='output4')(inputs4)
        # inputs5= Concatenate()([lstm0, lstm1, lstm2, lstm3, lstm4])
        inputs5 = concatenate([lstm0, lstm1, lstm2, lstm3, lstm4])
        inputs5 = Reshape(target_shape=(self.N + 4, 1))(inputs5)
        lstm5 = LSTM(units=1, name='output5')(inputs5)
        # dense_inputs = Concatenate()([lstm0, lstm1, lstm2, lstm3, lstm4, lstm5])
        lstm6_inputs = concatenate([lstm1, lstm2, lstm3, lstm4, lstm5])
        lstm6_inputs = Reshape(target_shape=(self.n, 1))(lstm6_inputs)
        lstm6 = LSTM(units=self.n)(lstm6_inputs)
        self.model = Model(inputs=inputs1, outputs=lstm6)

    def create_layers_lstm_N_to_N_to_n(self):
        inputs1 = Input(shape=(self.data.sequence_len, 1), name='input1')
        lstm1 = LSTM(units=self.N)(inputs1)
        lstm2 = LSTM(units=self.N)(Reshape(target_shape=(self.N, 1))(lstm1))
        # outputs = LSTM(units=self.n, return_sequences=True)(Reshape(target_shape=(self.N, 1))(lstm2))
        outputs = Dense(units=self.n)(lstm2)

        self.model = Model(inputs=inputs1, outputs=outputs)

    def create_layers_lstm_N_to_half_N_to_n(self):
        inputs1 = Input(shape=(self.data.sequence_len, 1), name='input1')
        lstm1 = LSTM(units=self.N)(inputs1)
        lstm2 = LSTM(units=int(self.N/2))(Reshape(target_shape=(self.N, 1))(lstm1))
        # outputs = LSTM(units=self.n)(Reshape(target_shape=(int(self.N/2), 1))(lstm2))
        outputs = Dense(units=self.n)(lstm2)

        self.model = Model(inputs=inputs1, outputs=outputs)

    def create_layers_lstm_N_to_half_N_to_n_y1(self):
        inputs1 = Input(shape=(self.data.period_len, 1), name='input1')
        lstm1 = LSTM(units=self.N)(inputs1)
        lstm2 = LSTM(units=int(self.N/2))(Reshape(target_shape=(self.N, 1))(lstm1))
        # outputs = LSTM(units=self.n)(Reshape(target_shape=(int(self.N/2), 1))(lstm2))
        outputs = Dense(units=1)(lstm2)

        self.model = Model(inputs=inputs1, outputs=outputs)

    def create_layers_lN_tod_tolhalfN_to_n(self):
        inputs1 = Input(shape=(self.data.sequence_len, 1), name='input1')
        lstm1 = LSTM(units=self.N)(inputs1)
        dropout1 = Dropout(rate=0.5)(lstm1)
        lstm2 = LSTM(units=int(self.N / 2))(Reshape(target_shape=(self.N, 1))(dropout1))
        # outputs = LSTM(units=self.n)(Reshape(target_shape=(int(self.N/2), 1))(lstm2))
        outputs = Dense(units=self.n)(lstm2)

        self.model = Model(inputs=inputs1, outputs=outputs)

    def create_layers_ldldld(self):
        inputs1 = Input(shape=(self.data.sequence_len, 1), name='input1')
        lstm1 = LSTM(units=self.N)(inputs1)
        dropout1 = Dropout(rate=0.5)(lstm1)
        lstm2 = LSTM(units=self.N)(Reshape(target_shape=(self.N, 1))(dropout1))
        dropout2 = Dropout(rate=0.5)(lstm2)
        lstm3 = LSTM(units=int(self.N / 2))(Reshape(target_shape=(self.N, 1))(dropout2))
        # outputs = LSTM(units=self.n)(Reshape(target_shape=(int(self.N/2), 1))(lstm3))
        outputs = Dense(units=self.n)(lstm3)

        self.model = Model(inputs=inputs1, outputs=outputs)


def custom_loss(y_true, y_pred):
    return soft_dtw(y_true, y_pred)
        # return soft_dtw(y_true, y_predicted, gamma=1.0)


train = TrainModel()
df.sequence_len = df.period_len
train.N = df.period_len
train.n = 3
train.data = df
train.create_layers_lstm_N_to_N_to_n()


'''tensor1 = tf.Variable([1, 2, 3])
tensor2 = tf.Variable([2, 3])
print('soft dtw tensors', soft_dtw(tensor1, tensor2))'''



target_tensor = tf.Variable(initial_value=[0, 0, 0])

train.model.compile(optimizer='RMSprop', loss=custom_loss, target_tensors=target_tensor)

'''train.model.fit({'input1': df.x_train},
                {'output1': df.y_train[:, 0], 'output2': df.y_train[:, 1], 'output3': df.y_train[:, 2], 'output4': df.y_train[:, 3], 'output5': df.y_train[:, 4]},
                epochs=20, batch_size=100)'''

train.model.fit(x=df.x_train, y=df.y_train[:, 0], epochs=100, batch_size=32)
train.model.summary()

# train.model.save('model_api_2_.h5')
print(train.model.predict(df.x_test))

# test_loss, test_acc = train.model.evaluate(df.x_test, df.y_test)

# print('\nТочность на проверочных данных:', test_acc)
predicted_stock_price = np.array(train.model.predict(df.x_test))
print(predicted_stock_price)
real_stock_price = df.y_test
new_shape = int(predicted_stock_price.shape[0]) * int(predicted_stock_price.shape[1])

s = 0
p = 0
m = 0
for i in range(0, predicted_stock_price.shape[0]):
    if np.sign(sum(predicted_stock_price[i])) == np.sign(sum(real_stock_price[i])):
        s += 1
    if np.sign(sum(real_stock_price[i])) > 0:
        p += 1
    else:
        m += 1
    # if np.sign(predicted_stock_price[i, df.n - 1] - predicted_stock_price[i, 0]) == np.sign(real_stock_price[i, df.n - 1] - real_stock_price[i, 0]):
    #    s += 1
S = s / predicted_stock_price.shape[0]
print(S, p, m)

a = np.reshape(predicted_stock_price, newshape=new_shape)
b = np.reshape(real_stock_price, newshape=new_shape)
plt.plot(b, color='black')
plt.plot(a, color='green')
plt.xlabel('Time')
plt.legend()
plt.show()


