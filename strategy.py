import keras
import keras.losses
from pywt import dwt, idwt

def custom_loss(y_true, y_predicted):
    return abs(y_true - y_predicted)


keras.losses.custom_loss = custom_loss

new_model = keras.models.load_model('model_api_76.h5')
new_model.compile(optimizer='adam', loss=custom_loss)
new_model.summary()

# data = pd.read_csv('1h_BTC_USD_Bitstamp.csv')