# -*- encoding: utf-8 -*-

from os import walk, makedirs
from os.path import exists, join
from calendar import timegm
from time import strptime, strftime, time
from datetime import datetime

import pandas as pd

from cryptocompare import get_historical_price_minute as get_minute_data, get_historical_price_hour as get_hour_data, get_historical_price_day as get_day_data


def get_data(fpath, coin, market, timeframe, exchange, **kwargs):
    sec_format = '%Y-%m-%d %H:%M:%S'
    timeframe = timeframe.lower()

    folder = join(fpath, coin + '_' + market)
    dfile = join(folder, '{}_{}_{}_{}.csv'.format(timeframe, exchange, coin, market))

    old_data = None
    if exists(dfile):
        old_data = pd.read_csv(dfile, sep=',', parse_dates=True, index_col=0)

    start_date = kwargs.get('start_date', '2010-01-01 00:00:00')
    last_date = kwargs.get('last_date', datetime.now().strftime(sec_format))

    update_data = kwargs.get('update_data', False)
    if update_data or old_data is None:
        start_date = strptime(start_date, sec_format)
        start_date_s = int(timegm(start_date))

        last_date = strptime(last_date, sec_format)
        last_date_s = int(timegm(last_date))

        date_diff = last_date_s - start_date_s
        if timeframe == '1m':
            limit = date_diff / 60
            get_data_func = get_minute_data
        elif timeframe == '1h':
            limit = date_diff / 60 / 60
            get_data_func = get_hour_data
        elif timeframe == '1d':
            limit = date_diff / 60 / 60 / 24
            get_data_func = get_day_data
        else:
            raise Exception('Unsupported timeframe exception: ' + timeframe)

        limit = int(limit) + 100
        data = get_data_func(coin=coin, curr=market, limit=limit, timestamp=last_date_s, exchange=exchange)

        data = data.loc[data.index >= strftime(sec_format, start_date)]
        data['Volume'] = data.Volumeto
        data = data.drop('Volumeto', axis=1)

        if old_data is not None:
            old_data = old_data.to_dict(orient='index')
            old_data.update(data.to_dict(orient='index'))
            data = pd.DataFrame.from_dict(old_data, orient='index')
            data = data.loc[data.index.drop_duplicates()]

        if not exists(folder):
            makedirs(folder)

        data = data.sort_index()
        data.to_csv(dfile, sep=',')
    else:
        data = old_data.loc[(old_data.index >= start_date) & (old_data.index <= last_date)]

    return data